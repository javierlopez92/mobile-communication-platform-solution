package com.solution.app.commons;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.solution.app.commons.McpConstants.JSONKEYS;
import com.solution.app.commons.McpConstants.MESSAGESTATUS;
import com.solution.app.commons.McpConstants.MESSAGETYPE;
import com.solution.app.commons.McpConstants.STATUSCODE;
import com.solution.app.models.entity.McpCallJsonObject;
import com.solution.app.models.entity.McpFile;
import com.solution.app.models.entity.McpGenericJsonObject;
import com.solution.app.models.entity.McpMsgJsonObject;

public class JsonImporter {

	public static void importMcpJsonFromFile (McpFile mcpFile, File file) throws Exception
	{
		/* Create the McpCallJsonObject or McpMsgJsonObject */
		final List<McpGenericJsonObject> res = new LinkedList<>();
		
		FileReader fr = null;
		final JSONParser parser = new JSONParser();
		try {
			fr = new FileReader(file);
			final BufferedReader br = new BufferedReader(fr);
			
			String line;
			while ((line = br.readLine()) != null) {
				if (line.isEmpty()) continue;
				final JSONObject jsonObject = (JSONObject) parser.parse(line);
				final McpGenericJsonObject mcpJsonObject = generateMcpJsonObject(mcpFile, jsonObject);
				
				if (mcpJsonObject == null) continue;
				else res.add(mcpJsonObject);
			}
			
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
			fr.close();
		} catch (ParseException e) {
			e.printStackTrace();
			fr.close();
		}
	}
	
	private static McpGenericJsonObject generateMcpJsonObject (McpFile mcpFile, JSONObject jsonObject)
	{
		/* Get message type */
		final MESSAGETYPE messageType = MESSAGETYPE.parseString((String) jsonObject.get(JSONKEYS.MESSAGETYPE.getName()));
		
		if (messageType == null)
			return null;
		
		McpGenericJsonObject mcpObject;
		
		/* Get rest of parameters */
		final Long timestamp = parseLong(jsonObject.getOrDefault(JSONKEYS.TIMESTAMP.getName(), null));
		final Long origin = parseLong(jsonObject.getOrDefault(JSONKEYS.ORIGIN.getName(), null));
		final Long destination = parseLong(jsonObject.getOrDefault(JSONKEYS.DESTINATION.getName(), null));
		final Long duration = parseLong(jsonObject.getOrDefault(JSONKEYS.DURATION.getName(), null));
		final STATUSCODE statusCode = STATUSCODE.parseString((String) jsonObject.get(JSONKEYS.STATUSCODE.getName()));
		final String statusDescription = (String) jsonObject.get(JSONKEYS.STATUSDESCRIPTION.getName());
		final String msgContent = (String) jsonObject.get(JSONKEYS.MESSAGECONTENT.getName());
		final MESSAGESTATUS msgStatus = MESSAGESTATUS.parseString((String) jsonObject.get(JSONKEYS.MESSAGE_STATUS.getName()));
		if (messageType.equals(MESSAGETYPE.CALL))
		{
			mcpObject = new McpCallJsonObject(mcpFile, timestamp, origin, destination, 
					duration, statusCode, statusDescription, jsonObject);
			
		} else if (messageType.equals(MESSAGETYPE.MSG))
		{
			mcpObject = new McpMsgJsonObject(mcpFile, timestamp, origin, destination, 
					duration, msgStatus, msgContent, jsonObject);
		} else
			return null;
		
		/* Compute metrics */
		boolean hasMissingFields = false;
		if (timestamp == null || origin == null || destination == null || duration == null) hasMissingFields = true;
		if (mcpObject.isCallObject())
		{
			if (statusCode.isUnkown() || statusDescription == null) hasMissingFields = true;
		}
		else if (mcpObject.isMsgObject())
		{
			if (msgContent == null || msgStatus.isNull()) hasMissingFields = true;
		}
		
		boolean hasFieldsWithErrors = false;
		if ((timestamp != null && timestamp == -1L) || (origin != null && origin == -1L)
				|| (destination != null && destination == -1L) || (duration != null && duration == -1L)) hasFieldsWithErrors = true;
		if (mcpObject.isCallObject() && statusCode.isUnkown()) hasFieldsWithErrors = true;
		else if (mcpObject.isMsgObject() && msgStatus.isUnkown()) hasFieldsWithErrors = true;
		
		boolean hasBlackContent = false;
		if (mcpObject.isMsgObject() && msgContent.isEmpty()) hasBlackContent = true;
		
		mcpObject.setHasMissingFields(hasMissingFields);
		mcpObject.setHasFieldsWithErrors(hasFieldsWithErrors);
		mcpObject.setHasBlackContent(hasBlackContent);
		
		return mcpObject;
	}
	
	/* -1 if wrong format, null if missing */
	public static Long parseLong (Object o) {
		if (o == null) return null;
		if (o instanceof Long) return (Long) o;
		return null;
	}
}
