package com.solution.app.commons;

public class McpConstants {

	public static enum MESSAGETYPE 
    { 
    	CALL, MSG;
    	public boolean isCall () { return this == CALL; }
    	public boolean isMsg () { return this == MSG; }
    	public static MESSAGETYPE parseString (String code) {
    		if (code == null) return null;
    		else if (code.equals(CALL.name())) return CALL;
    		else if (code.equals(MSG.name())) return MSG;
    		else return null;
    	}
    };
    
    public static enum STATUSCODE 
    { 
    	OK, KO, UNKNOWN, NULL;
    	public boolean isOk () { return this == OK; }
    	public boolean isKo () { return this == KO; }
    	public boolean isUnkown () { return this == UNKNOWN; }
    	public boolean isNull () { return this == NULL; }
    	public static STATUSCODE parseString (String code) {
    		if (code == null) return NULL;
    		else if (code.equals(OK.name())) return OK;
    		else if (code.equals(KO.name())) return KO;
    		else return UNKNOWN;
    	}
    };
    
    public static enum MESSAGESTATUS
    {
    	DELIVERED, SEEN, UNKNOWN, NULL;
    	public boolean isDelivered () { return this == DELIVERED; }
    	public boolean isSeen () { return this == SEEN; }
    	public boolean isUnkown () { return this == UNKNOWN; }
    	public boolean isNull () { return this == NULL; }
    	public static MESSAGESTATUS parseString (String code) {
    		if (code == null) return NULL;
    		else if (code.equals(DELIVERED.name())) return DELIVERED;
    		else if (code.equals(SEEN.name())) return SEEN;
    		else return UNKNOWN;
    	}
    }
    
    public static enum JSONKEYS
    {
    	MESSAGETYPE("message_type"), TIMESTAMP("timestamp"), ORIGIN ("origin"),
    	DESTINATION("destination"), DURATION("duration"), STATUSCODE("status_code"),
    	STATUSDESCRIPTION("status_description"), MESSAGECONTENT("message_content"),
    	MESSAGE_STATUS("message_status");
    	
    	private final String name;
		private JSONKEYS(String name) { this.name = name; }
		public String getName() { return this.name; }
    }
    
    public static enum METRICS
    {
    	ROWSMISSINGFIELDS ("Number of rows with missing fields"), 
    	MSGSWITHBLACKCONTENT ("Number of messages with black content"),
    	ROWSWITHFIELDSERRORS ("Number of rows with fields errors"), 
    	CALLSORGDSTBYCOUNTRYCODE ("Number of calls origin/destination grouped by country code"),
    	RELBETWEENOKKO ("Relationship between OK/KO calls (%)"),
    	AVERAGECALLDURATIONBYCODE ("Average call duration grouped by country code"),
    	WORDOCURRENCEINMSGCONTENT ("Word ocurrence ranking in message content");
    	
    	private final String name;
		private METRICS(String name) { this.name = name; }
		public String getName() { return this.name; }
    }
    
    public static enum KPIS
    {
    	NUMPROCESSEDJSONFILES ("Total number of processed JSON files"), 
    	NUMOFROWS ("Total number of rows"), 
    	NUMOFCALLS ("Total number of calls"), 
    	NUMOFMESSAGES ("Total number of messages"), 
    	NUMOFORIGINCOUNTRYCODES ("Total number of different origin country codes"), 
    	NUMOFDESTINATIONCOUNTRYCODES ("Total number of different destination country codes"),
    	DURATIONOFJSONPROCESS ("Duration of each JSON process");
    	
    	private final String name;
    	private KPIS(String name) { this.name = name; }
    	public String getName() { return this.name; }
    }
}
