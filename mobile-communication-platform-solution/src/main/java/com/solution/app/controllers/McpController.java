package com.solution.app.controllers;

import java.io.File;
import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.solution.app.commons.JsonImporter;
import com.solution.app.models.entity.McpFile;
import com.solution.app.models.service.IMcpService;

@Controller
public class McpController {

	@Autowired
	private IMcpService service;
	
	/* Add stored JSONs */
	@PostConstruct
    private void postConstruct() {
		final File json1 = new File(JsonImporter.class.getResource("/static/json/MCP_20180131.json").getFile());
		final File json2 = new File(JsonImporter.class.getResource("/static/json/MCP_20180201.json").getFile());
		final File json3 = new File(JsonImporter.class.getResource("/static/json/MCP_20180202.json").getFile());
		
		try {
			this.service.addMcpFile(new McpFile(json1));
			this.service.addMcpFile(new McpFile(json2));
			this.service.addMcpFile(new McpFile(json3));
		}
		catch (Exception ex) {
		}
    }
	
	@GetMapping(value= {"/", "/home"})
	public String index (Model model) {
		model.addAttribute("title", "List of MCP files");
		model.addAttribute("mcpFiles", service.getAllMcpFiles());
		return "index";
	}
	
	@GetMapping(value="/{id}")
	public String details(@PathVariable(value="id") Long id, Model model) {
		final McpFile mcpFile = service.getMcpFileById(id);
		if (mcpFile == null) return "redirect:/home";
		model.addAttribute("title", "MCP File '" + id + "' details");
		model.addAttribute("mcpFile", mcpFile);
		return "details";
	}
	
	@GetMapping(value="/metrics/{id}")
	public String metrics(@PathVariable(value="id") Long id, Model model) {
		final SortedSet<String> words = new TreeSet<>(Arrays.asList("ARE","YOU","FINE", "HELLO", "NOT"));
		final McpFile mcpFile = service.getMcpFileById(id);
		if (mcpFile == null) return "redirect:/home";
		model.addAttribute("title", "MCP File '" + id + "' metrics");
		model.addAttribute("mcpFile", mcpFile);
		model.addAttribute("genericMetrics", mcpFile.getGenericMetrics());
		model.addAttribute("callsByCountryCodes", mcpFile.getCallsByCountryCode());
		model.addAttribute("avgCallDurationByCountryCodes", mcpFile.getAvgCallDurationByCountryCode());
		model.addAttribute("wordsOcurrenceMap", mcpFile.getOcurrencesGivenWords(words));
		return "metrics";
	}

	@GetMapping(value="/kpis/{id}")
	public String kpis(@PathVariable(value="id") Long id, Model model) {
		final McpFile mcpFile = service.getMcpFileById(id);
		if (mcpFile == null) return "redirect:/home";
		model.addAttribute("title", "MCP File '" + id + "' KPIs");
		model.addAttribute("mcpFile", mcpFile);
		model.addAttribute("kpis", mcpFile.getKpis());
		return "kpis";
	}
}
