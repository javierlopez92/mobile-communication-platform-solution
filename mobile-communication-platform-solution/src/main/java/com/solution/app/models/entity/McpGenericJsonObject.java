package com.solution.app.models.entity;

import java.io.Serializable;

import org.json.simple.JSONObject;

import com.solution.app.commons.McpConstants.MESSAGETYPE;

public abstract class McpGenericJsonObject implements Serializable {

	private static final long serialVersionUID = 1L;

	/* MCP file owner */
	private final McpFile mcpFile;
	
	/* Message type (CALL or MSG) */
	private final MESSAGETYPE msgType;
	
	/* Timestamp */
	private final Long timestamp;
	
	/* Origin */
	private final Long origin;
	
	/* Destination */
	private final Long destination;
	
	/* Duration */
	private final Long duration;
	
	/* JSON Object */
	private final JSONObject jsonObject;
	
	/* Metrics values */
	private boolean hasMissingFields;
	private boolean hasBlackContent;
	private boolean hasFieldsWithErrors;

	McpGenericJsonObject(McpFile mcpFile, MESSAGETYPE msgType, Long timestamp, Long origin, Long destination,
			Long duration, JSONObject jsonObject) {
		this.msgType = msgType;
		this.timestamp = timestamp;
		this.origin = origin;
		this.destination = destination;
		this.duration = duration;
		this.jsonObject = jsonObject;
		this.mcpFile = mcpFile;
		this.hasMissingFields = false;
		this.hasBlackContent = false;
		this.hasFieldsWithErrors = false;
		mcpFile.addJsonObjec(this);
	}

	public Long getMcpFileId () {
		return this.mcpFile.getId();
	}

	public MESSAGETYPE getMsgType () {
		return msgType;
	}

	public Long getTimestamp () {
		return timestamp;
	}

	public Long getOrigin () {
		return origin;
	}

	public Long getDestination () {
		return destination;
	}

	public Long getDuration () {
		return duration;
	}

	public JSONObject getJsonObject () {
		return jsonObject;
	}
	
	public Boolean isValid () { return !hasMissingFields() && !hasBlackContent() && !hasFieldsWithErrors(); }
	
	public Boolean isCallObject () { return this.msgType.isCall(); }
	public Boolean isMsgObject () { return this.msgType.isMsg(); }
	public McpCallJsonObject getAsCallJsonObject () { 
		if (!isCallObject()) return null;
		return (McpCallJsonObject) this; 
	}
	
	public McpMsgJsonObject getAsMsgJsonObject () { 
		if (!isMsgObject()) return null;
		return (McpMsgJsonObject) this; 
	}
	
	public boolean hasMissingFields() {
		return hasMissingFields;
	}

	public void setHasMissingFields(boolean hasMissingFields) {
		this.hasMissingFields = hasMissingFields;
	}

	public boolean hasBlackContent() {
		return isMsgObject() ? hasBlackContent : false;
	}

	public void setHasBlackContent(boolean hasBlackContent) {
		this.hasBlackContent = hasBlackContent;
	}

	public boolean hasFieldsWithErrors() {
		return hasFieldsWithErrors;
	}

	public void setHasFieldsWithErrors(boolean hasFieldsWithErrors) {
		this.hasFieldsWithErrors = hasFieldsWithErrors;
	}	
}
