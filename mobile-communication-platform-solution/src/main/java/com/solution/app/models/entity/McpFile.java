package com.solution.app.models.entity;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.solution.app.commons.JsonImporter;
import com.solution.app.commons.McpConstants.KPIS;
import com.solution.app.commons.McpConstants.METRICS;

public class McpFile implements Serializable {

	private static final long serialVersionUID = 1L;

	/* ID of the MCP JSON file (generated day in YYYYMMDD format) */
	private final Long id;
	
	/* List of all JSONObjects defined */
	private final List<McpGenericJsonObject> mcpObjects;
	
	/* JSON file */
	private final File file;
	
	/* JSON file rows */
	private final Long rows;
	
	/* Generic Metrics */
	private final SortedMap<METRICS, Double> genericMetrics;
	
	/* Number of calls grouped by country metric */
	private final SortedMap<Integer, Long> callsByCountryMetric;
	
	/* Average call duration by country metric */
	private final SortedMap<Integer, Double> avgCallDurationByCountryMetric;
	
	/* KPIs */
	private final SortedMap<KPIS, Long> kpis;
	
	public McpFile (File file) throws Exception {
		this.mcpObjects = new LinkedList<>();
		this.genericMetrics = new TreeMap<>();
		this.callsByCountryMetric = new TreeMap<>();
		this.avgCallDurationByCountryMetric = new TreeMap<>();
		this.kpis = new TreeMap<>();
		
		/* Get id from file name (remove extension and MCP_ */
		final String fileDate = FilenameUtils.removeExtension(file.getName()).substring(4);
		try {
			this.rows = Files.lines(file.toPath()).count();
			this.id = Long.parseLong(fileDate);
			this.file = file;
			
			JsonImporter.importMcpJsonFromFile(this, file);
			computeGenericMetrics();
			computeMetricsGroupedByCountryCode();
			computeKpis();
		} catch (NumberFormatException ex) {
			throw new Exception("Wrong filename format");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	public Long getId () {
		return id;
	}

	public List<McpGenericJsonObject> getMcpObjects () {
		return mcpObjects;
	}
	
	void addJsonObjec (McpGenericJsonObject obj) { 
		assert !this.mcpObjects.stream().anyMatch(o->o.getMcpFileId() != obj.getMcpFileId());
		this.mcpObjects.add(obj); 
	}

	public File getFile () {
		return file;
	}
	
	public Long getRows () { return this.rows; }
	
	public String getFormattedDate () {
		final SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
	    Date date;
		try {
			date = formatter.parse(this.id.toString());
			return new SimpleDateFormat("dd/MM/yy").format(date);
		} catch (ParseException e) {
			return "[WRONG DATE]";
		}
	}
	
	public String getFileAsString () {
		String res = "";
	    try
	    {
	        res = new String ( Files.readAllBytes(Paths.get(file.toURI())) );
	    }
	    catch (IOException e)
	    {
	        return "Cannot read JSON file";
	    }
	    return res;
	}

	public SortedMap<METRICS, Double> getGenericMetrics () { return Collections.unmodifiableSortedMap(this.genericMetrics); }
	public SortedMap<Integer, Long> getCallsByCountryCode () { return Collections.unmodifiableSortedMap(this.callsByCountryMetric); }
	public SortedMap<Integer, Double> getAvgCallDurationByCountryCode () { return Collections.unmodifiableSortedMap(this.avgCallDurationByCountryMetric); }
	public SortedMap<KPIS, Long> getKpis () { return Collections.unmodifiableSortedMap(this.kpis); }
	
	public void computeGenericMetrics () {
		this.genericMetrics.clear();
		if (mcpObjects.isEmpty()) return;
		
		final Long numOfRowsWithMissingFields = this.mcpObjects.stream().filter(o->o.hasMissingFields()).count();
		final Long numOfMsgWithBlackContent = this.mcpObjects.stream().filter(o->o.hasBlackContent()).count();
		final Long numOfRowsWithFieldErrors = this.mcpObjects.stream().filter(o->o.hasFieldsWithErrors()).count();
		final Long numOfOkCalls = this.mcpObjects.stream().filter(o->o.isCallObject()).filter(o->o.getAsCallJsonObject().isOkCall()).count();
		final Long numOfKoCalls = this.mcpObjects.stream().filter(o->o.isCallObject()).filter(o->o.getAsCallJsonObject().isKoCall()).count();
		final Double avgOkKo = (numOfOkCalls.doubleValue() / numOfKoCalls.doubleValue()) * 100.0;
		
		/* Save metrics */
		this.genericMetrics.put(METRICS.ROWSMISSINGFIELDS, numOfRowsWithMissingFields.doubleValue());
		this.genericMetrics.put(METRICS.MSGSWITHBLACKCONTENT, numOfMsgWithBlackContent.doubleValue());
		this.genericMetrics.put(METRICS.ROWSWITHFIELDSERRORS, numOfRowsWithFieldErrors.doubleValue());
		this.genericMetrics.put(METRICS.RELBETWEENOKKO, avgOkKo);
	}
	
	public void computeMetricsGroupedByCountryCode () {
		this.callsByCountryMetric.clear();
		this.avgCallDurationByCountryMetric.clear();
		if (mcpObjects.isEmpty()) return;
		
		final SortedMap<Integer, Long> totalDurationByCountryMap = new TreeMap<>();
		
		/* Get metrics searching by country code */
		final PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		for (McpGenericJsonObject mcpObject : mcpObjects.stream().filter(o->o.isCallObject()).collect(Collectors.toList())) {
			final McpCallJsonObject callObject = mcpObject.getAsCallJsonObject();
			final Long origin = callObject.getOrigin();
			final Long destination = callObject.getDestination();
			final Long duration = callObject.getDuration();
			if (origin == null || destination == null || duration == null) continue;
			try {
				final Phonenumber.PhoneNumber originNumber = phoneUtil.parse("+" + origin.toString(), "");
				final Phonenumber.PhoneNumber destinationNumber = phoneUtil.parse("+" + destination.toString(), "");
				final int originCountryCode = originNumber.getCountryCode();
				final int destinationCountryCode = destinationNumber.getCountryCode();
				if (originCountryCode != destinationCountryCode) continue; /* Call between two countries */
				
				incrementMap(callsByCountryMetric, originCountryCode, 1L);
				incrementMap(totalDurationByCountryMap, originCountryCode, duration);
			} catch (NumberParseException ex) {
				continue;
			}
		}
		
		/* Calculate avg duration */
		for (Entry<Integer, Long> entry : totalDurationByCountryMap.entrySet()) {
			final Integer countryCode = entry.getKey();
			final Long totalDuration = entry.getValue();
			final Long numberOfCallsInThisCc = callsByCountryMetric.get(countryCode);
			final Double avgDuration = totalDuration.doubleValue() / numberOfCallsInThisCc.doubleValue();
			this.avgCallDurationByCountryMetric.put(countryCode, avgDuration);
		}
	}
	
	private void computeKpis () {
		this.kpis.clear();
		
		final Integer numOfJsonFiles = this.mcpObjects.size();
		final Long numOfRows = this.getRows();
		final Long numCalls = this.mcpObjects.stream().filter(o->o.isCallObject()).count();
		final Long numMsgs = this.mcpObjects.stream().filter(o->o.isMsgObject()).count();
		final Long durationOfJsonProcess = -1L; //Didn't understand that, sorry
		
		final Set<Integer> originCountryCodes = new HashSet<>();
		final Set<Integer> destinationCountryCodes = new HashSet<>();
		final PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		for (McpGenericJsonObject mcpObject : mcpObjects) {
			try {
				
				final Long origin = mcpObject.getOrigin();
				if (origin != null)
				{
					final Phonenumber.PhoneNumber originNumber = phoneUtil.parse("+" + origin.toString(), "");
					final int originCountryCode = originNumber.getCountryCode();
					originCountryCodes.add(originCountryCode);
				}

				final Long destination = mcpObject.getDestination();
				if (destination != null)
				{
					final Phonenumber.PhoneNumber destinationNumber = phoneUtil.parse("+" + destination.toString(), "");
					final int destinationCountryCode = destinationNumber.getCountryCode();
					destinationCountryCodes.add(destinationCountryCode);
				}
			} catch (NumberParseException ex) {
				continue;
			}
		}
		
		/* Save kpis */
		this.kpis.put(KPIS.NUMPROCESSEDJSONFILES, numOfJsonFiles.longValue());
		this.kpis.put(KPIS.NUMOFROWS, numOfRows);
		this.kpis.put(KPIS.NUMOFCALLS, numCalls);
		this.kpis.put(KPIS.NUMOFMESSAGES, numMsgs);
		this.kpis.put(KPIS.NUMOFORIGINCOUNTRYCODES, (long) originCountryCodes.size());
		this.kpis.put(KPIS.NUMOFDESTINATIONCOUNTRYCODES, (long) destinationCountryCodes.size());
		this.kpis.put(KPIS.DURATIONOFJSONPROCESS, durationOfJsonProcess);
	}
	
	public SortedMap<String, Long> getOcurrencesGivenWords (SortedSet<String> words) {
		final SortedMap<String, Long> res = new TreeMap<>();
		final List<McpMsgJsonObject> allMsgObjects = this.getMcpObjects().stream().filter(o->o.isMsgObject())
				.map(o->o.getAsMsgJsonObject()).collect(Collectors.toList());
		words.forEach(w->res.put(w.trim(), allMsgObjects.stream()
				.filter(m->m.getMsgContent().toLowerCase().contains(w.trim().toLowerCase())).count()));
		
		return res;
	}
	
	private static <T> void incrementMap (SortedMap<T, Long> map, T key, Long increment)
	{
		final Long currentValue = map.getOrDefault(key, 0L);
		final Long newValue = currentValue + increment;
		map.put(key, newValue);
	}
}
