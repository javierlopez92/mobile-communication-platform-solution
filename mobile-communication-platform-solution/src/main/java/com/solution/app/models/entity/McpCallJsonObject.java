package com.solution.app.models.entity;


import org.json.simple.JSONObject;

import com.solution.app.commons.McpConstants.MESSAGETYPE;
import com.solution.app.commons.McpConstants.STATUSCODE;

public class McpCallJsonObject extends McpGenericJsonObject{

	private static final long serialVersionUID = 1L;

	/* Call Status code */
	private final STATUSCODE callStatusCode;
	
	/* Call Status description */
	private final String callStatusDescription;
	
	public McpCallJsonObject(McpFile mcpFile, Long timestamp, Long origin, Long destination,
			Long duration, STATUSCODE callStatusCode, String callStatusDescription, JSONObject jsonObject) {
		super (mcpFile, MESSAGETYPE.CALL, timestamp, origin, destination, duration, jsonObject);

		this.callStatusCode = callStatusCode;
		this.callStatusDescription = callStatusDescription;
	}

	public STATUSCODE getCallStatusCode() {
		return callStatusCode;
	}

	public String getCallStatusDescription() {
		return callStatusDescription;
	}
	
	public boolean isOkCall () { return callStatusCode.isOk(); }
	public boolean isKoCall () { return callStatusCode.isKo(); }
}
