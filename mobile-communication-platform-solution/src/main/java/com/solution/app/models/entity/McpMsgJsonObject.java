package com.solution.app.models.entity;


import org.json.simple.JSONObject;

import com.solution.app.commons.McpConstants.MESSAGESTATUS;
import com.solution.app.commons.McpConstants.MESSAGETYPE;

public class McpMsgJsonObject extends McpGenericJsonObject {
	
	private static final long serialVersionUID = 1L;

	/* Message Status */
	private final MESSAGESTATUS msgStatus;

	/* Message Content */
	private final String msgContent;
	
	
	public McpMsgJsonObject(McpFile mcpFile, Long timestamp, Long origin, Long destination,
			Long duration, MESSAGESTATUS msgStatus, String msgContent, JSONObject jsonObject) {
		super (mcpFile, MESSAGETYPE.MSG, timestamp, origin, destination, duration, jsonObject);

		this.msgContent = msgContent;
		this.msgStatus = msgStatus;
	}

	public MESSAGESTATUS getMsgStatus() {
		return msgStatus;
	}

	public String getMsgContent() {
		return msgContent;
	}
}
