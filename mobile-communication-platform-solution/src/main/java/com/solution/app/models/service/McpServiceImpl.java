package com.solution.app.models.service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.solution.app.models.entity.McpFile;

@Service
public class McpServiceImpl implements IMcpService {
	
	private List<McpFile> mcpFiles = new LinkedList<>();

	@Override
	public List<McpFile> getAllMcpFiles() {
		return Collections.unmodifiableList(mcpFiles);
	}

	@Override
	public void addMcpFile(McpFile mcpFile) {
		assert !mcpFiles.stream().anyMatch(f->f.getId() == mcpFile.getId());
		mcpFiles.add(mcpFile);
	}

	@Override
	public McpFile getMcpFileById(Long id) {
		final Optional<McpFile> mcpFile = mcpFiles.stream().filter(f->Long.compare(f.getId(), id) == 0).findFirst();
		return mcpFile.isPresent() ? mcpFile.get() : null;
	}

	
	
}
