package com.solution.app.models.service;

import java.util.List;

import com.solution.app.models.entity.McpFile;

public interface IMcpService {

	public List<McpFile> getAllMcpFiles ();
	public void addMcpFile (McpFile mcpFile);
	public McpFile getMcpFileById (Long id);
}
