package com.solution.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobileCommunicationPlatformSolutionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobileCommunicationPlatformSolutionApplication.class, args);
	}

}
